document.addEventListener('DOMContentLoaded', function () {
    // Nuevo Vue 
    new Vue({
        el: '#appReservas',
        data() {
            return {
                Data: {
                    id: + new Date(),
                    content: '',
                    start: '',
                    end: '',
                },
                year: '',
                Reservas: []
            }
        },
        methods: {
            // Funcion para crear una reserva
            crearReservas: function () {
                var dateString = this.year;
                console.log("fecha ", convertDateFormat(dateString));
                // Invertimos el formato de la fecha
                function convertDateFormat(string) {
                    var info = string.split('-');
                    return info[2] + '/' + info[1] + '/' + info[0];
                }

                // Empujamos los datos de la reserva cargada
                this.Reservas.push({
                    id: + new Date(),
                    content: this.Data.content,
                    start: this.year + ' ' + this.Data.start,
                    end: this.year + ' ' + this.Data.end,
                });

                // Vaciamos el formulario de añadir
                this.Data.content = '';
                this.Data.start = '';
                this.Data.end = '';
                this.year = ''

                //Se envian los datos de la reserva al archivo a la variable items del archivo script 
                items.add(this.Reservas);
                this.Reservas.add(items)


            },
            borrarReservas: function (Reservas_id) {
                // Borramos de la lista
                this.Reservas.splice(Reservas_id, 1);
            },

        }

    });
});


